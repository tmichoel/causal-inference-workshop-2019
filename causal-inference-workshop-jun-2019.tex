\documentclass[10pt]{beamer}

% \usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{amsmath,amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{marvosym}
\usepackage{hyperref}
\usepackage{soul}
\usepackage{tikz}
\usepackage{wasysym}
\usepackage{multirow}
%\usepackage{enumitem}
\usepackage{MnSymbol}
\usepackage{pifont}

\usetikzlibrary{arrows,shapes.misc}

% setlist[itemize]{label={\footnotesize $\bullet$}, topsep=-0.2ex,
 %  partopsep=0pt, parsep=1pt, itemsep=0pt, leftmargin=3.5mm,
 %  labelsep=0.5ex} 

\newtheorem{thm}{Theorem}

\newcommand{\indep}{\upmodels}
\newcommand{\dep}{\nupmodels}
%\newcommand{\Rr}{\mathbb{R}}
\newcommand{\Nr}{\mathbb{N}}
\newcommand{\C}{\mathbb{C}}
%\newcommand{\R}{\mathcal{R}}
\newcommand{\Q}{\mathcal{Q}}
%\renewcommand{\L}{\mathcal{L}}
\renewcommand{\S}{\mathcal{S}}
%\newcommand{\N}{\mathcal{N}}
%\newcommand{\G}{\mathcal{G}}
\newcommand{\Hg}{\mathcal{H}}
\newcommand{\V}{\mathcal{V}}
%\newcommand{\E}{\mathcal{E}}
%\newcommand{\pa}{\text{Pa}}
\newcommand{\D}{\mathcal{D}}

\newcommand{\pa}{\text{Pa}}
\newcommand{\G}{\mathcal{G}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\N}{\mathcal{N}}
\newcommand{\B}{\mathbf{B}}
\newcommand{\X}{\mathbf{X}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\Gb}{\mathbf{G}}
\newcommand{\Pb}{\mathbf{P}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\I}{\mathbb{1}}
\newcommand{\mbf}{\boldsymbol\mu}
\newcommand{\Sbf}{\boldsymbol\Sigma}
\newcommand{\Obf}{\boldsymbol\Omega{}}

\newcommand{\Hnull}{\mathcal{H}_{\text{null}}}
\newcommand{\Halt}{\mathcal{H}_{\text{alt}}}
\newcommand{\PT}[1]{$\text{P}_\text{#1}$}

%\newcommand{\strong}[1]{{\color{red}#1}}
\newcommand{\balph}{\boldsymbol{\alpha}}
\DeclareMathOperator*{\argmax}{argmax}
\DeclareMathOperator*{\iid}{i.i.d.}



\definecolor{frias}{rgb}{0,0.616,0.580}%{0,0.627,0.627}%{0,0.729,0.71}

\definecolor{uibr}{rgb}{0.811,0.235,0.227}
\definecolor{uibg}{rgb}{0.541,0.671,0.404}

\newcommand{\strong}[1]{{\color{uibr}#1}}

\newcommand{\chm}{{\large \color{green}{\smiley}}}
\newcommand{\crs}{{\large\color{red}\frownie}}

\usetheme{Malmoe}
\usecolortheme[named=uibr]{structure}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{}
\setbeamertemplate{enumerate subitem}{{\normalsize(\alph{enumii})}}

\graphicspath{{./}{../../Logos/}{../../Figures/}{../../2012/CRI-2012/}}

\title{\textcolor{black}{Causal inference in systems genetics}}

\subtitle{\textcolor{black}{\\{Workshop on causal inference in complex
      marine ecosystems}}}

\author{Tom Michoel}

\date{12 June 2019}

\begin{document}

\begin{frame}
  \thispagestyle{empty}

  \vspace*{-7mm}
  \hspace*{-10mm}\includegraphics[width=\paperwidth]{uib-header}
  

  \titlepage

  
\end{frame}

\begin{frame}
  \frametitle{What is systems genetics?}
    \fontsize{9}{11}\selectfont
  \begin{columns}
    \begin{column}{.3\linewidth}
      \begin{center}
        \includegraphics[width=.8\linewidth]{healthy_heart_1}

        $\vdots$
        
        \includegraphics[width=.8\linewidth]{healthy_heart_2}

        \fontsize{5}{6}\selectfont
        {\tiny \url{https://www.nhs.uk/conditions/nhs-health-check/check-your-heart-age-tool/}}
      \end{center}
    \end{column}
    \begin{column}{.05\linewidth}
      \fontsize{25}{30}\selectfont
      \begin{align*}
        \Rightarrow
      \end{align*}
    \end{column}
    \begin{column}{0.4\linewidth}
      \begin{minipage}[c]{\linewidth}
        \includegraphics[width=\linewidth]{figNet}
      \end{minipage}
      \begin{center}
        {\tiny [Schadt. Nature (2009)]}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Overview of the talk}
  
  \begin{columns}
    \begin{column}{0.4\linewidth}
      \begin{minipage}[c]{\linewidth}
        \includegraphics[width=\linewidth]{figNet}
      \end{minipage}
    \end{column}
    \begin{column}{0.65\linewidth}
      \begin{enumerate}
      \item A crash course in genetics \& molecular biology\\[3mm]
      \item Causal inference and Bayesian network reconstruction with
        1000s of variables. \\[3mm]
      \item A (very quick) case study in coronary artery
        disease\\[3mm]
      \item Some general thoughts
      \end{enumerate}
    \end{column}
  \end{columns}
\end{frame}

\part{A crash course in genetics \& molecular biology}
\frame{\partpage}

\begin{frame}
  \frametitle{Genome-wide association studies (GWAS) identify genetic
    variants associated with complex traits and diseases}
  \vspace*{-5mm}
  \begin{center}
    Genetic variation \emph{causes} variation in complex traits.  
  \end{center}
  \begin{center}
    \includegraphics[width=.4\linewidth]{ng784-F1}
    \end{center}
  \begin{itemize}
  \item Why \emph{``causes''}?
    \begin{itemize}
    \item Correlation $\neq$ causation.
    \item Treatment randomly assigned vs.\ self-selected.
    \item Genotype is fixed from conception and randomly distributed in population w.r.t. lifestyle and environment.\\[2mm]
    \end{itemize}
  \item How does genetic variation cause trait variation?
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Gene expression determines cellular states}
  \begin{columns}
    \begin{column}{.4\linewidth}
      \begin{center}
        \includegraphics[width=\linewidth]{central_dogma}\\
        {\tiny [Wikipedia]}\\[3mm]

        \includegraphics[width=\linewidth]{paulsson1a}
      \end{center}
    \end{column}
    \begin{column}{.6\linewidth}
      \begin{itemize}
      \item Genes are transcribed into mRNA and translated into protein.
      \item Every step in this process can be regulated by genetic or environmental factors.
      \item The repertoire and relative levels of proteins expressed determines cellular identity, fate, cell-to-cell communication, etc.
      \item Variation in cellular properties causes variation in phenotypes.
      \end{itemize}
    \end{column}
  \end{columns}
  \bigskip
  \begin{center}
    Genetic variation causes  trait variation by affecting gene expression.\\[3mm]

    \strong{How does genetic variation affect mRNA expression?}
    
  \end{center}
  
\end{frame}

\begin{frame}
  \frametitle{Genetic variation causes variation in transcription
    factor binding}
  \begin{itemize}
  \item TFs bind to accessible regulatory DNA elements to control gene transcription. 
  \item Environmental perturbations affect TF concentrations in nucleus.
  \item Genetic perturbations affect ability of TFs to bind to DNA.
  \end{itemize}
  \begin{center}
    \includegraphics[width=.75\linewidth]{albert2016-fig1B-2}\\
     {\tiny [Albert and Kruglyak, Nat Rev Genet (2016)]}
  \end{center}
\end{frame}


\begin{frame}
  \frametitle{Genes are organized in hierarchical, multi-tissue causal networks}
  \fontsize{9}{10.5}\selectfont
  \begin{columns}
    \begin{column}{.5\linewidth}
      \begin{center}
      \includegraphics[width=\linewidth]{civelek_nrg_fig3a}\\
      {\tiny [Civelek and Lusis, Nat Rev Genet (2014)]}

      \vspace*{5mm}

      \includegraphics[width=\linewidth]{albert2016-fig3A}\\
       {\tiny [Albert and Kruglyak, Nat Rev Genet (2016)]}
    \end{center}
    \end{column}
    \begin{column}{.6\linewidth}
      \begin{itemize}
      \item Variation in expression of one gene has downstream
        consequences on expression of other genes.
      \item Example: Introduction of just 4 TFs (``Yamanaka factors'')
        converts adult cells into stem cells.
      \item Hundreds to thousands of genes are differentially expressed between cellular states (e.g.\ healthy vs.\ disease).
      \item Gene expression in one tissue can affect gene expression
        in other tissues.
      \item Phenotype variation also causes gene expression variation
        (``environmental'' perturbation).
      \end{itemize}
    \end{column}
  \end{columns}
  \fontsize{10}{12}\selectfont
  \medskip
  \begin{center}
    \strong{Inference (reconstruction) of causal gene networks is
      essential to understand how the genotype determines the
      phenotype.}
  \end{center}
\end{frame}


\begin{frame}
  \frametitle{Genome-wide transcriptome variation studies map the
    genetic architecture of gene expression}
  \fontsize{9}{11}\selectfont
  \begin{columns}
    \begin{column}{.3\linewidth}
      \includegraphics[width=\linewidth]{GTEx_diagram}

      \bigskip

      \includegraphics[width=\linewidth]{nrg2969-f1}
    \end{column}
    \begin{column}{.79\linewidth}
      \begin{itemize}
      \item eQTL $=$ expression QTL $=$ genetic variant associated
        with gene expression level.
      \item Strongest effects in gene proximity (regulatory region).
      \item RNA-sequencing measures expression levels of $\sim$40k
        transcripts in a single sample.
      \item Gene expression is highly tissue-specific, but only few
        tissues are easily accessible (blood, immune cells).
      \item Genotype-Tissue Expression project (GTEx): 48 tissues, 620
        donors (deceased), 10,294 samples $\to$ 341,316 eGenes (31,403
        unique)
      \end{itemize}

      \vspace*{5mm}

      \includegraphics[width=\linewidth]{GTEx_tissues_2.png}
      \begin{center}
        {\tiny [GTEx website (2017)]}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}




\part{Causal inference in systems genetics}
\frame{\partpage}

\begin{frame}
  \frametitle{Genome-transcriptome variation studies inform on causal
    interactions}
  \begin{itemize}
  \item Genetic variation between individuals precedes gene expression
    variation $\Rightarrow$ eQTLs act as instrumental variables to
    infer causal direction between correlated expression traits.
  \item Molecular version of a random trial.

    \bigskip

    \begin{center}
      \includegraphics[width=0.7\linewidth]{civelek_nrg_fig3a}\\
      {\tiny [Civelek and Lusis, Nat Rev Genet (2015)]}
      
      % \vspace*{15mm}

    \bigskip
    
    \includegraphics[width=\linewidth]{rockman2008_fig2}\\
    {\tiny [Rockman, Nature (2008)]}
   \end{center}
  \end{itemize}

 
\end{frame}

\begin{frame}
  \frametitle{Genome-transcriptome variation studies inform on causal
    interactions}
  \fontsize{10}{11}\selectfont
  \begin{itemize}
  \item \textit{Cis}-eQTLs act as genetic instruments to infer the causal
    direction between coexpressed genes
    \begin{center}
      \includegraphics[width=.6\linewidth]{civelek_nrg_fig3a}
    \end{center}
  \item Main differences with ``standard'' causal inference/Mendelian
    randomization:\\[.5mm]
    \begin{itemize}
    \item Thousands of traits are analyzed simultaneously:
      \begin{itemize}
      \item Computationally challenging \crs\\[.5mm]
      \item Bayesian inference of real vs.\ null distributions \chm\\[1mm]
      \end{itemize}
    \item eQTL effect sizes are often large \chm\\[1mm]
    \item Pleiotropy is rare \chm\\[1mm]
    \item Confounding by shared upstream regulators is common:\\ FFL is
      most abundant motif in gene regulatory networks \crs
    \end{itemize}
    \vspace*{-12mm}
    \hspace{.92\linewidth}\includegraphics[width=.12\linewidth]{ffl}
  \end{itemize}
\end{frame}



\begin{frame}
  \frametitle{Traditional causal inference in genome-wide studies is
    based on a conditional independence test}
  \fontsize{9}{11}\selectfont 

  \vspace*{3mm}
  Given a pair of gene expression traits $A$, $B$, and  a
  \textit{cis}-eQTL $E$ of gene $A$, \textbf{Chen, Emmert-Streib and Storey
    (Genome Biol 2007)} estimate:
  \vspace*{1mm}
  \begin{align*}
    P(E\to A\to B) = P(E\to A)\times P(E\to B) \times P(B\perp E\mid A) 
  \end{align*}
  \vspace*{-7mm}
  \begin{center}
    \hspace*{-3mm}\includegraphics[width=.9\linewidth]{findr-equation-1-annot}
  \end{center}

  % \hspace*{.5\linewidth}\includegraphics[width=.5\linewidth]{civelek_nrg_fig3a}   

  \vspace*{-2mm}
  \textbf{Approach:} For each factor:\\
  \begin{enumerate}
  \item Likelihood ratio between null and alternative model.\\[1mm]
  \item Probability (1-FDR) that alternative hypothesis is true, given
    LLR test  statistics in real and randomly permuted data.\\[1mm]
  \end{enumerate}
  \begin{columns}
    \begin{column}[c]{.7\linewidth}
      \fontsize{8}{9}\selectfont
      \vspace*{-3mm}
       \begin{align*}
         p_{\text{obs}}(LLR) &= \alpha\, p(LLR \mid \Hnull) +
                               (1-\alpha)\,  p(LLR \mid \Halt) \\[2mm]
         P(\Halt\mid LLR)    &=1-\alpha\;\frac{p(LLR \mid
                               \Hnull)}{p_{\text{obs}}(LLR)},\; p(LLR\to 0\mid \Halt)=0
      \end{align*}
      \begin{center}
        \fbox{
          \begin{minipage}{.8\linewidth}
            $p_{\text{obs}}(LLR)$ from testing 1000s of B's for each
            $A$.\\[2mm]
            $p(LLR \mid \Hnull)$ from randomly permuted data.
          \end{minipage}
        }
      \end{center}
      
    \end{column}
    \begin{column}[c]{.36\linewidth}
      \includegraphics[width=\linewidth]{findr-fig-histogram}
    \end{column}
  \end{columns}
 \end{frame}

 \begin{frame}
   \frametitle{LLRs are computed using linear models and ML 
     parameter  estimates} 
   \fontsize{9}{11}\selectfont 
  

  \begin{center}
    \begin{tabular}{lll}
      \multicolumn{2}{l}{\textbf{Association between $E$ and $A$}} & \\[2mm]
     Alternative model: & $A\mid E \sim
    \N(\mu_E,\sigma_A^2) $  & \multirow{2}{*}{$LLR = \log \dfrac{\prod_i p(a_{i}\mid
              e_i, \hat\mu_{e_i},\hat\sigma_A^2)}{\prod_i p(a_{i}\mid
              \hat\mu,\hat\sigma^2)}$}\\[2mm]
      Null model: & $A \sim
    \N(\mu,\sigma^2)\perp E $\\[8mm]
      \multicolumn{2}{l}{\textbf{$B$ independent of $E$ given $A$}}\\[2mm]
      Alternative model: & $A\mid E \sim
                                                    \N(\mu^A_E,\sigma_A^2)$\\[1mm]
      & $B\mid A \sim \N(a\, x_A + b,\sigma_{BA}^2)$\\[3mm]
      Null model: & \multicolumn{2}{l}{$A,B \mid E \sim \N\left(
                  \begin{bmatrix}
                    \mu^A_E\\
                    \mu^B_E
                  \end{bmatrix},
   \begin{bmatrix}
     \sigma_A^2 & \rho_{AB} \sigma_A\sigma_B\\
     \rho_{AB} \sigma_A\sigma_B & \sigma_B^2
   \end{bmatrix} \right)$}%\\[8mm]
      % & \multicolumn{2}{l}{$LLR = \log \dfrac{\prod_i p(a_{i}\mid e_i,
      %         \hat\mu_{e_i},\hat\sigma_a^2) p(b_i\mid a_i, \hat a,
      %   \hat b, \hat)}{\prod_i p(a_{i}\mid
      %         \hat\mu,\hat\sigma_0^2)}$}
    \end{tabular}  
  \end{center}
  
  \begin{center}
    \includegraphics[width=.35\linewidth]{findr-fig-tests-EAB}
 \end{center}
 \end{frame}


 \begin{frame}
   \frametitle{LLR null distributions for finite sample size can be
     computed analytically}
   
   \fontsize{9}{10.5}\selectfont 

   \textbf{Wilk's theorem} states that in the limit of infinite sample
   size, under the null hypothesis, $2\,LLR \sim \chi^2(\Delta df)$.
   At current sample sizes ($n=100-1000$), the $\chi^2$ approximation
   is not good enough, and random permutations of the data are used
   typically.\\[3mm]

   We derived \textbf{analytic null distributions} for finite $n$. For
   instance for $E\to A$ test:\\[3mm]
   \begin{itemize}
   \item $LLR=-\dfrac{n}2\ln \hat\sigma_A^2$,
     $\hat\sigma_A^2=1-\sum_{j=0}^{n_a}\frac{n_j}{n}\hat\mu_j^2$\\[3mm]
   \item Under null hypothesis $A_i\sim\iid \N(0,1)$ and standardized
     to have $\frac1n\sum_i A_i=0$ and $\frac1n\sum_i A_i^2=1$.
   \end{itemize}
   
   \medskip
   %\vspace*{-20mm}
   \hspace*{90mm}
   \begin{minipage}{.2\linewidth}
     \begin{center}
       \includegraphics[width=.75\linewidth]{lingfei_wang}\\  
        {\fontsize{6}{9} Lingfei Wang}
     \end{center}
   \end{minipage}

 \end{frame}

 \begin{frame}
   \fontsize{9}{10.5}\selectfont 
   Then
   \begin{align*}
     \frac1n LLR(A_1,\dots,A_n) 
     &=-\dfrac{1}2\ln \left(1-\frac{Y_2}{Y_2+Y_3}\right),\quad
     Y_2 \sim \chi^2(n_a),\quad
     Y_3 \sim \chi^2(n-n_a-1) \perp Y_2\\[2mm]
     Y&=\frac{Y_2}{Y_2+Y_3} \sim \text{Beta}\left(\tfrac{n_a}2,\tfrac{n-n_a-1}2\right)
   \end{align*}
   P.d.f. of $Z=-\frac12\ln(1-Y)$ with
   $Y\sim \text{Beta}(\frac{k_1}2,\frac{k_2}2)$ is
   \begin{align*}
     p(z\mid k_1,k_2) = \frac{2}{B(\tfrac{k_1}2, \tfrac{k_2}2)} 
     (1-e^{-2z})^{\frac{k_1}2-1} e^{-k_2z}
   \end{align*}
   \vspace*{-6mm}
   \begin{center}
     \includegraphics[width=.6\linewidth]{eg4}
   \end{center}

   \vspace*{-32mm}
   \hspace*{92mm}
   \begin{minipage}{.2\linewidth}
     \begin{center}
       \includegraphics[width=.75\linewidth]{lingfei_wang}\\  
        {\fontsize{6}{9} Lingfei Wang}
     \end{center}
   \end{minipage}
 \end{frame}

\begin{frame}
  \frametitle{Mediation allows causal model selection\\
    only in the absence of hidden confounders}

  \vspace*{-10mm}
  \hspace*{81mm}\includegraphics[width=.35\linewidth]{civelek_nrg_fig3a}

  \fontsize{9}{11}\selectfont 
  \begin{center}
    
    \fontsize{7}{11}\selectfont
  \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (1,1) {$M_1$};
    \node (X) at (1,0) {$A$};
    \node (Y) at (2,0) {$B$};
    \node (E) at (0,0) {$E_A$};
    \path[->,thick] (E) edge (X);
    \path[->,thick] (X) edge (Y);
  \end{tikzpicture} \quad\textit{vs.}\quad
  \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (1,1) {$M_2$};
    \node (X) at (1,0) {$A$};
    \node (Y) at (2,0) {$B$};
    \node (E) at (0,0) {$E_A$};
    \path[->,thick] (E) edge (X);
    \path[->,thick] (Y) edge (X);
  \end{tikzpicture}\; or\;
  \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (.5,1) {$M_3$};
    \node (X) at (1,0.5) {$A$};
    \node (Y) at (1,-0.5) {$B$};
    \node (E) at (0,0) {$E_A$};
    \path[->,thick] (E) edge (X);
    \path[->,thick] (E) edge (Y);
  \end{tikzpicture}\; or\;
  \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (1,1) {$M_4$};
    \node (X) at (1,0) {$A$};
    \node (Y) at (2,0) {$B$};
    \node (E) at (0,0) {$E_A$};
    \path[->,thick] (E) edge[out=30,in=150] (Y);
    \path[->,thick] (Y) edge (X);
  \end{tikzpicture}
  \end{center}
  \begin{enumerate}
  \item \strong{\textit{Trans-association:}} Statistical dependence
    between $E_A$ and $B$ excludes model $M_2$.\\[2mm]
  \item \strong{\textit{Mediation:}} Statistical independence between
    $E_A$ and $B$ conditioned on $A$ excludes model $M_3$ and $M_4$.
  \end{enumerate}
  \begin{center}
    \strong{BUT}

    \bigskip

    Mediation fails in the presence of hidden confounders due to
    collider effect.
  \end{center}
  
  \begin{center}
    \fontsize{9}{11}\selectfont 
    \begin{tikzpicture}[baseline=2mm,scale=.8]
      \node[shape=circle,inner sep=1pt,draw] (X) at (1,0) {$A$};
      \node (Y) at (2.,0) {$B$};
      \node (E) at (0.,0.) {$E_A$};
      \node (H) at (1.5,1) {$H$};
      \path[->,thick] (E) edge (X);
      \path[->,thick] (X) edge (Y);
      \path[->,thick,dashed] (H) edge (X);
      \path[->,thick,dashed] (H) edge (Y);
    \end{tikzpicture}\;$\Rightarrow$\;
    \begin{tikzpicture}[baseline=2mm,scale=.8]
      \node (Y) at (2.,0) {$B$};
      \node (E) at (0.,0.) {$E_A$};
      \node (H) at (1.5,1) {$H$};
      \path[-,thick,dashed] (H) edge (E);
      \path[->,thick,dashed] (H) edge (Y);
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Non-independence allows causal model\\
    selection with hidden confounders}

  \vspace*{-10mm}
  \hspace*{81mm}\includegraphics[width=.35\linewidth]{civelek_nrg_fig3a}

  \fontsize{9}{11}\selectfont 
  \begin{center}
    \begin{tikzpicture}[baseline=-1mm,scale=.8]
      \node[shape=circle,inner sep=0pt,draw] (M) at (1.5,2) {$M_1$};
      \node (X) at (1,0) {$A$};
      \node (Y) at (2.,0) {$B$};
      \node (E) at (0.,0.) {$E_A$};
      \node (H) at (1.5,1) {$H$};
      \path[->,thick] (E) edge (X);
      \path[->,thick] (X) edge (Y);
      \path[->,thick,dashed] (H) edge (X);
      \path[->,thick,dashed] (H) edge (Y);
    \end{tikzpicture}\quad\textit{vs.}\quad
    \begin{tikzpicture}[baseline=-1mm,scale=.8]
      \node[shape=circle,inner sep=0pt,draw] (M) at (1.5,2) {$M_2$};
      \node (X) at (1,0) {$A$};
      \node (Y) at (2.,0) {$B$};
      \node (E) at (0.,0.) {$E_A$};
      \node (H) at (1.5,1) {$H$};
      \path[->,thick] (E) edge (X);
      \path[->,thick,dashed] (H) edge (X);
      \path[->,thick,dashed] (H) edge (Y);
    \end{tikzpicture}\; or\;
    \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (1.5,2) {$M_3$};
    \node (X) at (1,0) {$A$};
    \node (Y) at (2,0) {$B$};
    \node (E) at (0,0) {$E_A$};
    \node (H) at (1.5,1) {$H$};
    \path[->,thick] (E) edge (X);
    \path[->,thick] (Y) edge (X);
    \path[->,thick,dashed] (H) edge (X);
      \path[->,thick,dashed] (H) edge (Y);
  \end{tikzpicture}\; or\;
  \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (.5,2) {$M_4$};
    \node (X) at (1,0.5) {$A$};
    \node (Y) at (1,-0.5) {$B$};
    \node (E) at (0,0) {$E_A$};
    \path[->,thick] (E) edge (X);
    \path[->,thick] (E) edge (Y);
  \end{tikzpicture}
  \end{center}
  \begin{enumerate}
  \item \strong{\textit{Trans-association:}} Statistical dependence
    between $E_A$ and $B$ excludes model $M_2$ and $M_3$.\\[2mm]
  \item \strong{\textit{Non-independence:}} Statistical dependence
    between $A$ and $B$ conditioned on $E_A$ excludes model $M_4$.
  \end{enumerate}
  \vspace*{-5mm}
  \begin{center}
    \strong{BUT}
    
    \smallskip
    
    Non-independence fails if there is a hidden confounder in model
    $M_4$.\\[2mm]

    \fontsize{9}{11}\selectfont 
    \begin{tikzpicture}[baseline=-1mm,scale=.8]
      \node (X) at (1,0.5) {$A$};
      \node (Y) at (1,-0.5) {$B$};
      \node (E) at (0,0) {$E_A$};
      \node (H) at (2,0) {$H$};
      
      \path[->,thick] (E) edge (X);
      \path[->,thick] (E) edge (Y);
      \path[->,thick,dashed] (H) edge (X);
      \path[->,thick,dashed] (H) edge (Y);
    \end{tikzpicture}\\[2mm]

    \textit{Because eQTL-gene interactions are \textbf{local}, a
      direct effect of $E_A$ on $B$ can only occur if $A$ and $B$ are
      co-located on the genome.}
  \end{center}
\end{frame}



\begin{frame}
  \hspace*{-6mm}\includegraphics[width=1.1\linewidth]{findr_ploscb_header}
  
  \fontsize{8}{10}\selectfont 

  %\vspace*{-13mm}

  \begin{columns}
    \begin{column}{.58\linewidth}
     \includegraphics[width=1.2\linewidth]{findr_ploscb_title}%\\[3mm]
      \begin{itemize}
      \item Findr software implements 6 likelihood-ratio tests and
        outputs probabilities (1-FDR) of hypotheses being true.\\
      \item Analytic results to avoid random permutations, resulting
        in massive speed-up.\\
      \item Combining LLR tests gives probabilities of causal
        effects.\\
        \begin{itemize}
        \item \textbf{Mediation}: %\\[1mm] 
          $P = P_1\, P_2\, P_3$\\[2mm]
        \item \textbf{Non-independent \textit{trans}-effect}: \\[2mm] 
          $P = P_1\, P_2 \, P_5$ or $P_1\, (P_2 \, P_5 + P_4)$
      \end{itemize}
    \end{itemize}
    \end{column}
    \begin{column}{.47\linewidth}
      \hspace*{35mm}\includegraphics[width=.28\linewidth]{lingfei_wang}
%      \vspace*{25mm}
      \begin{center}
        \includegraphics[width=\linewidth]{findr-tests} 
      \end{center}
      \end{column}
  \end{columns}

  \vspace*{2mm}
  \begin{center}
    \href{https://github.com/lingfeiwang/findr}{{\small
            \texttt{https://github.com/lingfeiwang/findr}}}
  \end{center}

\end{frame}


% \begin{frame}
%   \frametitle{Findr accounts for weak secondary linkage, allows for
%      hidden confounders, and outperforms existing methods} 
%    \fontsize{9}{10.5}\selectfont 
%    \begin{center}
%      \includegraphics[width=.55\linewidth]{fig1-DREAM}
%    \end{center}
%    \vspace*{-2mm}
%    \begin{itemize}
%    \item \textbf{Mediation:} high specificity, but low sensitivity due
%      to hidden confounders, worse with increasing sample size.
%    \item \textbf{Non-independent effects:} much higher sensitivity at
%      potential cost of increase in false positives.
%    \end{itemize}

%     %\smallskip

%     \begin{center}
%       \fbox{
%         \begin{minipage}{.9\linewidth}
%           \begin{center}
%             In simulations and comparisons to known miRNA and
%             TF-targets, ``non-independent effects'' always performed better 
%             than ``mediation''.  
%           \end{center}
%         \end{minipage}
%       }
%     \end{center}

%  \end{frame}

 \begin{frame}
   \frametitle{Mediation-based causal inference  fails in the presence
     of hidden confounders and weak regulations}
   \fontsize{9}{10.5}\selectfont 
   \begin{center}
     DREAM5 Systems Genetics Challenge\\
     {\footnotesize Synthetic networks (1000 genes), simulated data\\
        (RILs and ODE model with \textit{cis} and \textit{trans} effects)}

     \includegraphics[width=.55\linewidth]{fig1-DREAM}
   \end{center}
   \only<1>{
     \begin{itemize}
     \item Traditional test performs worse than correlation test.
     \item Traditional test performs worse with increasing sample size.
     \item Inclusion of conditional independence worse than secondary
       linkage alone.
     \end{itemize}}
   \only<2>{
     Hypothesis: elevated false negative rate (FNR) due to:
     \begin{itemize}
     \item Weak secondary linkage $E\to B$ in true $E\to A\to B$
       relations.
     \item Conditional independence fails in the presence of
       confounders\\
       (common regulators) due to collider effect.
     \end{itemize}
     \vspace*{-18mm}
     \hspace*{.9\linewidth}\includegraphics[width=.15\linewidth]{fig-confound}
   }
 \end{frame}

 \begin{frame}
   \frametitle{Findr accounts for weak secondary linkage, allows for
     hidden confounders, and outperforms existing methods}
   \fontsize{9}{10.5}\selectfont 
   \begin{minipage}{.55\linewidth}
     \includegraphics[width=\linewidth]{fig1-DREAM}
   \end{minipage}\hfill
   \begin{minipage}{.43\linewidth}
     \includegraphics[width=\linewidth]{findr-tests}
   \end{minipage}
   
   % \begin{align*}
   %   P =  \tfrac12(P_2\times P_5 + P_4)\times P_1
   % \end{align*}

   % \vspace*{-2mm}

   \bigskip

   \begin{itemize}
   \item Relevance test ($P_4$) verifies that $B$ is not independent
     of $E$ and $A$ simultaneously, accounts for weak $E\to B$.\\[1mm]
   \item Secondary linkage ($P_2$) $\times$ controlled ($P_5$) tests
     ensure that correlation between $A$ and $B$ cannot be fully
     explained by $E$. \\[1mm]
   \item True $E\to A\to B$, with or without confounders, satisfy
     these tests.\\[1mm]
   \item Findr-P best performer on \textbf{all} DREAM5 datasets.
   \end{itemize}
 \end{frame}

 \begin{frame}
   \frametitle{Findr outperforms existing methods on human data, while
   being a million times faster}
 \fontsize{9}{10.5}\selectfont 
 \vspace*{-5mm}
   \begin{columns}
     \begin{column}[t]{.5\linewidth}
       \begin{center}
         \textbf{microRNA target prediction}\\
         Better than traditional tests and
machine learning methods.

         \includegraphics[width=.95\linewidth]{fig3-GEUVADIS-miRNA}
       \end{center}
     \end{column}
     \begin{column}[t]{.5\linewidth}
       \begin{center}
         \textbf{TF target prediction}\\
         More accurate FDR estimates.\\[4mm]
         \includegraphics[width=.95\linewidth]{fig4-GEUVADIS-TF}\\[4mm]

         {\footnotesize GEUVADIS data, 360 EUR LCL samples: 3172
           eGenes, 55 e-miRNAs, 23,722 targets}
         
       
       \end{center}
     \end{column}
   \end{columns}
   \begin{center}
     {\footnotesize
       miRLAB: 20 e-miRNAs with known targets\\
         ENCODE + siRNA: 20 + 6 eGenes (TFs)  with known targets}
   \end{center}
 \end{frame}

 \begin{frame}
   \frametitle{Reconstructing global causal Bayesian networks from
     pairwise causal inferences}
   
   \fontsize{9}{11}\selectfont
   
   \medskip

   \hspace*{-1mm}\textbf{Aim:} Develop $O(n^2)$ $/$ $O(n^2\log n)$
   algorithms for reconstructing Bayesian networks from
   genome-transcriptome variation data.\\[1mm]
  
%      \medskip
  
  \begin{columns}
    \begin{column}{.75\linewidth}
      % \vspace*{-10mm}
      \textbf{Steps:}
      \begin{itemize}
      \item Calculate $P_{ij}=P(L_i \to X_i \to X_j)$ from causal inference test.
      \item Rank edges by $P_{ij}$ and create DAG by
        adding ranked edges and incremental cycle detection
        ($O(m^{3/2})$ for $m$ edges; $O(n^2\log n)$ for all edges).
      \item Sparse Bayesian network from DAG via $n$ independent
        variable selection problems.
      \end{itemize}
    \end{column}
    \begin{column}{.2\linewidth}
      \includegraphics[width=\linewidth]{schadt2009_fig4_sub_mine.pdf}
    \end{column}
  \end{columns}
  
  \begin{center}
    \includegraphics[width=.67\linewidth]{fig_method}

    {\footnotesize [Lingfei Wang, Pieter Audenaert, TM. bioRxiv]}
  \end{center}
 \end{frame}

 

 \begin{frame}
   \frametitle{A generative model for genome-transcriptome variation
     data}

   \fontsize{9}{11}\selectfont

   We wish to infer a causal gene network (DAG) from genotype data
   $\E$ and gene expression data $\X$ for $n$ genes in $m$ independent
   samples.\\[2mm]

   Given a DAG $\G$ on $n$ genes we assume
   \begin{align*}
     p(\X,\E \mid \G) &\propto P(\E\mid \X,\G)\, p(\X\mid \G)\\[1mm]
     p(\X\mid \G) &= \prod_{k=1}^m\prod_{j=1}^n p\bigl(x_{jk}\mid
                    \{x_{ik}\colon i\in \pa_j\}\bigl) && \text{(linear Gaussian
                    BN)}\\[1mm]
     P(\E\mid \X,\G) &\propto \prod_j\prod_{i\in\pa_j} 
     P(L_i\to G_i\to G_j\mid E_i, X_i, X_j) && \text{(pairwise causal inference)}
   \end{align*}
   Then
   \begin{align*}
     \L = \log P(\G\mid \X,\E) = \log p(\X\mid \G) +
     \sum_j\sum_{i\in\pa_j} \log P_{ij} + \text{const} 
   \end{align*}
   The model makes $P_{ij}$ behave as a structure ``prior'' albeit one
   inferred from the data $\E,\X$.
 \end{frame}

 \begin{frame}
   \frametitle{Greedy likelihood optimization}

   \fontsize{9}{11}\selectfont 

   \begin{enumerate}
   \item Find a topological node ordering which locally optimizes
     $\sum_j\sum_{i\in\pa_j} \log P_{ij}$: \\%[1mm]
     \begin{itemize}
     \item Without sparsity constraints, $\L$ is maximized by a
       ``complete'' DAG with $n(n-1)/2$ edges.\\[.5mm]
     \item Such a DAG defines a node ordering $\prec$ where
       $i\prec j\Leftrightarrow i\in\pa_j$. \\[.5mm]
     \item $p(\X\mid \G)$ is invariant under permutations of the
       ordering. \\[.5mm]
     \item Finding the DAG/node ordering which maximizes
       $\sum_j\sum_{i\in\pa_j} \log P_{ij}$ is NP-hard with no known
       polynomial approximation algorithms with known error bound. \\[.5mm]
     \item We use a greedy algorithm, adding edges from highest to
       lowest $\log P_{ij}$, skipping those that would create a cycle.\\[2mm]
     \end{itemize}
   \item Maximize $\log p(\X\mid \G)$ for given node ordering, subject
     to a sparsity constraint: \\%[1mm]
     \begin{itemize}
     \item $n$ independent variable selection problems, one for each
       node regressed on its predecessors in the ordering. \\[.5mm]
     \item Sparsity from double-exponential (lasso) prior on
       regression coefficients of linear Gaussian network.
     \end{itemize}

   \end{enumerate}
 \end{frame}

 

\part{A (very quick) case study in coronary artery disease}
\frame{\partpage}

\begin{frame}
  \frametitle{The STAGE and STARNET studies}
  \begin{columns}
    \begin{column}{.5\linewidth}
      \fontsize{9}{11}\selectfont 

      Multi-organ expression profiling in 7 vascular and metabolic
      tissues of patients undergoing coronary artery bypass grafting
      surgery at Karolinska University Hospital/Tartu University
      Hospital\\[3mm]

      \textbf{STAGE}
      \begin{itemize}
      \item 109 individuals, SNP arrays
      \item 612 tissue expression arrays
      \end{itemize}
      {\fontsize{7}{9}\selectfont [Foroughi-Asl \textit{et al}, Circ
        Cardiovasc Genet 2015]}\\[3mm]
    
      \textbf{STARNET}
      \begin{itemize}
      \item 566 individuals, SNP arrays
      \item 3,786 tissue RNA-seq profiles
      \end{itemize}
      \qquad{\fontsize{7}{9}\selectfont [Franz\'en \textit{et al},
        Science 2016]}
    \end{column}
    \begin{column}{.58\linewidth}
      \includegraphics[width=\linewidth]{aad6970-TableS1}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}

  \vspace*{1mm}

  \hspace*{-4mm}\begin{minipage}{\linewidth}
    \includegraphics[width=1.1\linewidth]{cellsystems_title}
  \end{minipage}\\[2mm]


  \hspace*{-4mm}\begin{minipage}{.55\linewidth}
    \includegraphics[width=\linewidth]{cellsystems_graphabs}
  \end{minipage}~%
  \begin{minipage}{.45\linewidth}
    \includegraphics[width=.8\linewidth]{cellsystems_authors}\\[7mm]

    \includegraphics[width=1.2\linewidth]{cellsystems_highlights}
  \end{minipage}\\[1mm]

  \hspace*{-3mm}\includegraphics[width=.45\linewidth]{cellsystems_cite}

\end{frame}

\begin{frame}
  % \frametitle{Cross-tissue regulatory gene networks in coronary artery
  % disease}
\fontsize{8}{10}\selectfont 
  \begin{columns}
    \begin{column}{.5\linewidth}
      \vspace*{-4mm}
      \hspace*{-10mm}
      \begin{itemize}
      
      \item Multi-tissue clustering identified 171 co-expression
        clusters (94 tissue-specific/77 cross-tissue).
      \item 61 clusters associated to key CAD phenotypes (athero,
        cholesterol, glucose, CRP).
      \item 30 CAD-causal clusters (CAD risk enriched eSNPs/GWAS
        genes).
      \item 12 clusters conserved in mouse with same phenotype
        association in same tissue.
      \item Key drivers of athero-causal Bayesian gene networks
        validated by siRNA targeting in THP-1 foam cells.
      \end{itemize}
    \end{column}
    \begin{column}{.65\linewidth}
     

      \includegraphics[width=\linewidth]{Figure_1_Study_Overview}

     % \vspace*{-4mm}
      \begin{center}
        {\tiny [Talukdar \textit{et al}, Cell Systems (2016)]}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

\part{The end (almost)}
\frame{\partpage}

\begin{frame}
  \frametitle{Any lessons for causal inference in ecosystem
    modelling and elsewhere?}
   
  \fontsize{9}{11}\selectfont 

  Gene networks are no less complex than ecosystem networks (I presume).

  \medskip

  Progress made by \dots
  \begin{itemize}
  \item focussing first on inferring the presence/absence of causal
    interactions between \textbf{pairs of variables} (or simple
    subsystems), treating the rest of the universe as latent
    confounders, %\\[2mm]
  \item using all available prior domain knowledge in the causal
    diagrams.
  \end{itemize}

  %\medskip

  \begin{minipage}{.3\linewidth}
         \begin{tikzpicture}[baseline=-1mm,scale=.8]
           \fontsize{8}{10}\selectfont
          %\node[shape=circle,inner sep=0pt,draw] (M) at (1.5,2) {$M_1$};
          \node (X) at (1,0) {$A$};
          \node (Y) at (2.,0) {$B$};
          \node (E) at (0.,0.) {$E_A$};
          \node (H) at (1.5,1) {$H$};
          \path[->,thick] (E) edge (X);
          \path[->,thick] (X) edge (Y);
          \path[->,thick,dashed] (H) edge (X);
          \path[->,thick,dashed] (H) edge (Y);
          % \path[->,dashed,thick] (X) edge [out=60,in=120] (Y);
        \end{tikzpicture}
      \end{minipage} %\hspace*{5mm}
      \begin{minipage}{.3\linewidth}
        \begin{tikzpicture}[baseline=-5mm,scale=.9]
          \fontsize{8}{10}\selectfont
          \node (X) at (1,0) {$X$};
          \node (Y) at (2.4,0) {$Y$};
          \node (Ex) at (0.,0.) {$E_X$};
          \node (Ez) at (0.,-1) {$E_Z$};
          \node (Z) at (1.,-1) {$Z$};
          \path[->,thick] (Ex) edge (X);
          \path[->,thick] (X) edge node[above] {$a$} (Y);
          \path[<->,dashed,thick] (X) edge [out=60,in=120]  (Y);
          \path[<->,dashed,thick] (Z) edge [out=-10,in=-100] (Y);
          \path[<->,thick,dashed] (Ex) edge [out=210,in=150] (Ez);
          \path[->,thick] (Ez) edge (Z);
          \path[->,thick] (Z) edge (Y);
          \path[->,thick] (Z) edge (X);
          \path[<->,thick,dashed] (X) edge [out=210,in=150] (Z);
        \end{tikzpicture}
      \end{minipage}
      {\fontsize{8}{10}\selectfont
      $a=\dfrac{\rho_{E_X,Y}-\rho_{E_X,E_Z}\rho_{E_Z,Y}}{\rho_{E_X,X}-\rho_{E_X,E_Z}\rho_{E_Z,X}}$}

      \medskip

      But of course, we have \dots
      \begin{itemize}
      \item good (decent) instruments,
      \item model systems (petri dish, mice, etc.) where predictions
        can be tested,
      \item a sample size greater than one (there's only one earth\dots).
      \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{However, are we getting there?}
  \fontsize{9}{11}\selectfont
  \begin{columns}
    \begin{column}{.3\linewidth}
      \begin{center}
        \includegraphics[width=.8\linewidth]{healthy_heart_1}

        $\vdots$
        
        \includegraphics[width=.8\linewidth]{healthy_heart_2}

      \end{center}
    \end{column}
    \begin{column}{.05\linewidth}
      \fontsize{25}{30}\selectfont
      \begin{align*}
        \Rightarrow
      \end{align*}
    \end{column}
    \begin{column}{0.4\linewidth}
      \begin{minipage}[c]{\linewidth}
        \includegraphics[width=\linewidth]{figNet}
      \end{minipage}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Acknowledgements}
  \fontsize{9}{11}\selectfont
  \vspace*{-4mm}
  \begin{columns}
    \begin{column}[t]{.35\linewidth}
      \begin{block}{Group members}
        \textbf{Sean Bankier} (UoE)\\
        Pau Erola (UoE)\\
        Siddharth Jayaraman (UoE)\\
        \textbf{Lingfei Wang} (UoE)\\[1mm]
        Ramin Hasibi (UiB)\\
        Tor Helleland (UiB)\\
        Ammar Malik (UiB)\\
        Assem Maratova (UiB)
      \end{block}
      \begin{block}{Ghent University}
        \textbf{Pieter Audenaert}
      \end{block}
    \end{column}
    \begin{column}[t]{.33\linewidth}
      \begin{block}{Edinburgh}
        Ruth Andrew\\
        Andy Crawford\\
        Filippo Menolascina\\
        \textbf{Brian Walker}\\
      \end{block}
      \begin{block}{ISMMS/Karolinska}
        Ariella Cohain\\        
        \textbf{Hassan Foroughi Asl}\\
        Oscar Franz\'en\\
        \textbf{Husain Talukdar}\\
        Eric Schadt\\
        \textbf{Johan Bj\"orkegren}\\
      \end{block}
    \end{column}
    \begin{column}[t]{.34\linewidth}
      \vspace*{-7mm}

      \hspace*{16mm}\begin{minipage}{.6\linewidth}
        \includegraphics[width=\linewidth]{UiB-emblem_gray}
      \end{minipage}
      \vspace*{-2mm}
      \begin{flushright}        
        \includegraphics[width=.35\linewidth]{British_Heart_Foundation}\\%[5mm]

        \includegraphics[width=.67\linewidth]{mrc}\\[2mm]
        
        \includegraphics[width=\linewidth]{bbsrc-colour}\\[3mm]

        \hspace*{-21mm}\includegraphics[height=.85cm]{NIH}\\[5mm]

        
      \end{flushright}
      \hspace*{-11mm}\textbf{{\large http://lab.michoel.info}}
    \end{column}
  \end{columns}
  \vspace*{-18mm}
  \hspace*{-4mm}\begin{minipage}{.3\linewidth}
    \includegraphics[width=\linewidth]{Cbu_logo_black_transparent}
  \end{minipage}
\end{frame}

\begin{frame}
  \begin{quote}
    Fighting for the acceptance of Bayesian networks in AI was a
    picnic compared with the fight for [the acceptance of] causal diagrams [in
    statistics]. That battle is still ongoing, with a few remaining
    islands of resistance.
  \end{quote}
  \begin{flushright}
    Judea Pearl, in The Book of Why (pp.\ 132)
  \end{flushright}
\end{frame}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
